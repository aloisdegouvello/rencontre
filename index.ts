import { Rencontre } from './src/rencontre';

declare var process: any

// let result2 = (new Rencontre()).create({
//   characters: [
//     { quantity: 1, effectiveCharacterLevel: 1 }
//     , { quantity: 1, effectiveCharacterLevel: 1 }
//     , { quantity: 1, effectiveCharacterLevel: 1 }
//     , { quantity: 1, effectiveCharacterLevel: 1 }
//     , { quantity: 1, effectiveCharacterLevel: 1 }
//     , { quantity: 1, effectiveCharacterLevel: 1 }
//   ],
//   monsters: [
//     { quantity: 1, challengeRating: 1 }
//     , { quantity: 1, challengeRating: 1 }
//     , { quantity: 1, challengeRating: 1 }
//     , { quantity: 1, challengeRating: 1 }
//     , { quantity: 1, challengeRating: 1 }
//     , { quantity: 1, challengeRating: 1 }
//   ]
// });

const args = process.argv.slice(2);
let result = (new Rencontre()).create({
  characters: args[0].split(',').map((x: string) => ({ quantity: 1, effectiveCharacterLevel: x })),
  monsters: args[1].split(',').map((x: string) => ({ quantity: 1, challengeRating: x })),
});

console.log(result)