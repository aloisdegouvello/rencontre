# Rencontre

[Try it online!](https://stackblitz.com/edit/angular-1m9quu)

* Create a basic TS package: [Building a typescript library?](https://www.tsmean.com/articles/how-to-write-a-typescript-library/);
* Set TypeScript in VSCode: [Setting up node application with TypeScript in VS Code  —  using mocha, chai, mochawesome, gulp, travis](https://blogs.msdn.microsoft.com/nilayshah/2018/01/07/unit-testing-node-application-with-typescript-in-vs-code-%E2%80%8A-%E2%80%8A-using-mocha-chai-mochawesome-gulp-travis/)
* CD with Gitlab: [Continuous Deployment to npm using GitLab CI](https://www.exclamationlabs.com/blog/continuous-deployment-to-npm-using-gitlab-ci/)