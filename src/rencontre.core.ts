// Derived from versions by John Dells, Tiera Starr, and Arcady
// Code strongly inspired by http://www.d20srd.org/extras/d20encountercalculator/

export interface Monster {
    /**
     * Choose the number of monsters (or traps) encountered.
    */
    quantity: number;

    /**
     * Select the [challenge rating]{@link http://www.d20srd.org/srd/monsters/intro.htm#challengeRating} (CR) of the monsters (or traps).
    */
    challengeRating: number;
}

export interface Character {
    /**
     * Choose the number of player characters involved in the encounter.
    */
    quantity: number;
    
    /**
     * Select the [effective character level]{@link http://www.d20srd.org/srd/monstersAsRaces.htm#levelAdjustmentandEffectiveCharacterLevel} (ECL) of the characters.
    */
    effectiveCharacterLevel: number;
}

export interface Encounter {
    /**
     * Monsters & Traps
    */
    monsters: Array<Monster>;

    /**
     * Party & Individual XP
    */
    characters: Array<Character>;

    /**
     * Where the encounter takes place. 
    */
    terrain?: TerrainType;
}

/** Encounter Level, Difficulty & Treasure */
export interface EncounterResult {
    /**
     * This is the average ECL of a four-member party of characters. The
     * party level for a group of fewer than four characters will be lower
     * than the average ECL. The party level for a group of more than
     * four characters will be higher than the average ECL. See
     * Challenge Ratings and Encounter Levels on page 48 of the
     * Dungeon Master's Guide.
    */
    partyLevel: number;

    /**
    * This is the encounter level (EL) of an encounter comprised of the
    * monsters selected. See "Challenge Ratings and Encounter Levels"
    * on page 48 of the Dungeon Master's Guide.
    */
    encounterLevel: number;
    /**
     * This indicates the relative difficulty of the encounter. See
     * "Difficulty" on page 49 of the Dungeon Master's Guide for details.
    */
    difficulty: DifficultyType;

    /**
     * This is how many encounters of the indicated difficulty an
     * adventure should have. See "Difficulty" on page 49 of the
     * Dungeon Master's Guide for details. 
    */
    percentOfTotal: number;

    /** This is the [average treasure value]{@link http://www.d20srd.org/srd/treasure.htm#tableTreasureValuesperEncounter} of this encounter. */
    treasureValue: number;

    /** This is how far the spot checks start at. The value is in feet. */
    spotChecks?: string;

    /**
     * This field indicates the appropriate experience point (XP) award
     * for characters who overcome this encounter. See page 36 of the
     * [Dungeon Master]{@link http://www.wizards.com/default.asp?x=products/dndcore/177520000}'s Guide for details.
    */
   experiencePoints: Array<number>;
}

export const aTreasure = [
    300, 600, 900, 1200, 1600, 2000,
    2600, 3400, 4500, 5800, 7500,
    9800, 13000, 17000, 22000, 28000,
    36000, 47000, 61000, 80000, 87000,
    96000, 106000, 116000, 128000, 141000,
    155000, 170000, 187000, 206000, 227000,
    249000, 274000, 302000, 332000, 365000,
    401000, 442000, 486000, 534000
];

export enum DifficultyType {
    Unknown = "Unknown",
    Trivial = "Trivial",
    VeryEasy = "Very Easy",
    Easy = "Easy",
    Challenging = "Challenging",
    VeryDifficult = "Very Difficult",
    Overpowering = "Overpowering",
    Unbeatable = "Unbeatable",
}

export enum TerrainType {
    Desert = "Desert",
    DesertDunes = "Desert, Dunes",
    Dungeon = "Dungeon",
    dpForest = "Forest, Deep",
    mpForest = "Forest, Medium",
    spForest = "Forest, Sparse",
    gHills = "Hill, Gentle",
    rHills = "Hills, Rugged",
    Moor = "Marsh, Moor",
    Swamp = "Marsh, Swamp",
    Mount = "Mountains",
    Plains = "Plains",
    Urban = "Urban",
    cWater = "Water, Clear",
    mWater = "Water, Murky"
}
