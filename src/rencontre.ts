import {
  aTreasure
  , TerrainType
  , DifficultyType
  , Encounter
  , EncounterResult
} from './rencontre.core';

export class Rencontre {
  public create(encounter: Encounter): EncounterResult {
    const result = this.mCalculate(encounter)
    return result;
  }

  private mFilterInputLevels(strX: string) {
    if (strX == "1/2") return 1 / 2;
    if (strX == "1/3") return 1 / 3;
    if (strX == "1/4") return 1 / 4;
    if (strX == "1/6") return 1 / 6;
    if (strX == "1/8") return 1 / 8;
    if (strX == "1/10") return 1 / 10;
    return parseFloat(strX);
  }

  private RoundOnePlace(x: number) {
    return Math.round(x * 10) / 10;
  }

  private Log2(x: number) {
    return Math.LOG2E * Math.log(x);
  }

  private mCRtoPL(x: number) {
    var iReturn = 0;
    if (x < 2) iReturn = x;
    else iReturn = Math.pow(2, (x / 2));
    return iReturn;
  }

  private mPLtoCR(x: number) {
    var iReturn = 0;
    if (x < 2) iReturn = x;
    else iReturn = 2 * this.Log2(x);
    return iReturn;
  }

  private mDifference(x: number, y: number) {
    return 2 * (this.Log2(x) - this.Log2(y));
  }

  private mDifficulty(x: number) {
    var strReturn = "Unknown";
    if (x < -9) strReturn = "Trivial";
    else if (x < -4) strReturn = "Very Easy";
    else if (x < 0) strReturn = "Easy";
    else if (x <= 0) strReturn = "Challenging";
    else if (x <= 4) strReturn = "Very Difficult";
    else if (x <= 7) strReturn = "Overpowering";
    else strReturn = "Unbeatable";
    return strReturn;
  }

  private mPercentEnc(x: number) {
    var strReturn = "Unknown";
    if (x < -4) strReturn = "0%";
    else if (x < 0) strReturn = "10%";
    else if (x <= 0) strReturn = "50%";
    else if (x <= 4) strReturn = "15%";
    else if (x <= 7) strReturn = "5%";
    else strReturn = "0%";
    return strReturn;
  }

  private mPercentEncs(x: number) {
    // What percentage of an adventures encounters should be at this EL.
    var p = 0;
    if (x < 0) p = 50 + (x * 20);
    else if (x > 5) p = 15 - ((x - 5) * 5);
    else p = 50 - (x * 7);

    if (x <= 8 && x > 5 && p <= 2) p = 2; // special case guess.
    if (p < 0) p = 0;

    p = Math.round(p); // smooth it out a bit.
    return p + "%";
  }

  private mEven(x: number) {
    var iReturn = 2 * Math.floor(x / 2);
    if (x < iReturn) iReturn += -2;
    else if (x > iReturn) iReturn += 2;
    return iReturn;
  }

  private mExperience(x: number, y: number) {
    // x = PClevel y = monsterlevel
    var iReturn = 0;
    if (x < 3) x = 3;
    if ((x <= 6) && (y <= 1)) iReturn = 300 * y;
    else if (y < 1) iReturn = 0;

    // This formula looks nice, but 3.5 doesn't follow a smooth formula like 3.0 did.
    else iReturn = 6.25 * x * (Math.pow(2, this.mEven(7 - (x - y)) / 2)) * (11 - (x - y) - this.mEven(7 - (x - y)));

    // Below catches places where the formula fails for 3.5.
    if ((y == 4) || (y == 6) || (y == 8) || (y == 10) || (y == 12) ||
      (y == 14) || (y == 16) || (y == 18) || (y == 20)) {
      if (x <= 3) iReturn = 1350 * Math.pow(2, (y - 4) / 2);
      else if (x == 5 && y >= 6) iReturn = 2250 * Math.pow(2, (y - 6) / 2);
      else if (x == 7 && y >= 8) iReturn = 3150 * Math.pow(2, (y - 8) / 2);
      else if (x == 9 && y >= 10) iReturn = 4050 * Math.pow(2, (y - 10) / 2);
      else if (x == 11 && y >= 12) iReturn = 4950 * Math.pow(2, (y - 12) / 2);
      else if (x == 13 && y >= 14) iReturn = 5850 * Math.pow(2, (y - 14) / 2);
      else if (x == 15 && y >= 16) iReturn = 6750 * Math.pow(2, (y - 16) / 2);
      else if (x == 17 && y >= 18) iReturn = 7650 * Math.pow(2, (y - 18) / 2);
      else if (x == 19 && y >= 20) iReturn = 8550 * Math.pow(2, (y - 20) / 2);
    }
    if ((y == 7) || (y == 9) || (y == 11) || (y == 13) || (y == 15) ||
      (y == 17) || (y == 19)) {
      if (x == 6) iReturn = 2700 * Math.pow(2, (y - 7) / 2);
      if (x == 8 && y >= 9) iReturn = 3600 * Math.pow(2, (y - 9) / 2);
      if (x == 10 && y >= 11) iReturn = 4500 * Math.pow(2, (y - 11) / 2);
      if (x == 12 && y >= 13) iReturn = 5400 * Math.pow(2, (y - 13) / 2);
      if (x == 14 && y >= 15) iReturn = 6300 * Math.pow(2, (y - 15) / 2);
      if (x == 16 && y >= 17) iReturn = 7200 * Math.pow(2, (y - 17) / 2);
      if (x == 18 && y >= 19) iReturn = 8100 * Math.pow(2, (y - 19) / 2);
    }

    if (y > 20) iReturn = 2 * this.mExperience(x, y - 2);
    // recursion should end this in short order.
    // This method is clean, and ensures any errors in the above
    // formulas for 3.5 are accounted for.

    // Finally we correct for out of bounds entries, doing this last to cut space on the
    // above formulas.
    if (x - y > 7) iReturn = 0;
    else if (y - x > 7) iReturn = 0;

    return iReturn;
  }

  private mTreasure(x: number) {
    if (x > 40) x = 40; // Not a clean solution. But no idea what ELs above 20 should give.
    let x2 = Math.floor(x);
    let iReturn;
    if (x < 1) iReturn = x * aTreasure[0];
    else if (x > x2) iReturn = aTreasure[x2 - 1] + (x - x2) * (aTreasure[x2] - aTreasure[x2 - 1]);
    else iReturn = aTreasure[x2 - 1];

    return iReturn;
  }

  private xDy(x: number, y: number) {
    return this.xDyPz(x, y, 0);
  }

  private xDyPz(x: number, y: number, z: number) {

    //alert (x+"d"+y+"+"+z);
    for (x; x > 0; x--) {
      z += Math.round(Math.random() * y);
      //alert("X: "+x+" Z: "+z);
    }
    //alert ("Z: "+z);
    return z;
  }

  private findSpotDistance(terrainType: TerrainType) {
    let spotDist = "";
    var isTxt = false;

    switch (terrainType) {
      case TerrainType.dpForest:
      case TerrainType.rHills:
        spotDist = this.xDy(2, 6) * 10 + " (2d6x10)";
        break
      case TerrainType.spForest:
        spotDist = this.xDy(3, 6) * 10 + " (3d6x10)";
        break
      case TerrainType.Moor:
      case TerrainType.DesertDunes:
        spotDist = this.xDy(6, 6) * 10 + " (6d6x10)";
        break
      case TerrainType.Desert:
        spotDist = this.xDy(6, 6) * 20 + " (6d6x20)";
        break
      case TerrainType.Plains:
        spotDist = this.xDy(6, 6) * 40 + " (6d6x40)";
        break
      case TerrainType.mWater:
        spotDist = this.xDy(1, 8) * 10 + " (1d8x10)";
        break
      case TerrainType.mpForest:
      case TerrainType.Swamp:
        spotDist = this.xDy(2, 8) * 10 + " (2d8x10)";
        break
      case TerrainType.cWater:
        spotDist = this.xDy(4, 8) * 10 + " (4d8x10)";
        break
      case TerrainType.gHills:
        spotDist = this.xDy(2, 10) * 10 + " (2d10x10)";
        break
      case TerrainType.Mount:
        spotDist = this.xDy(4, 10) * 10 + " (4d10x10)";
        break
      case TerrainType.Dungeon:
        spotDist = "Line of sight and lighting";
        isTxt = true;
        break
      case TerrainType.Urban:
        spotDist = "when event triggered";
        isTxt = true;
        break
      default:
        spotDist = "ERROR";
        isTxt = true;
    }
    if (isTxt == true) return spotDist;
    else return spotDist + " feet away";
  }

  private mCalculate(encounter: Encounter) {
    var iCount = 0;

    var iParty1Number = encounter.characters[0].quantity;
    var iParty2Number = encounter.characters[1].quantity;
    var iParty3Number = encounter.characters[2].quantity;
    var iParty4Number = encounter.characters[3].quantity;
    var iParty5Number = encounter.characters[4].quantity;
    var iParty6Number = encounter.characters[5].quantity;

    var iParty1Level = encounter.characters[0].effectiveCharacterLevel;
    var iParty2Level = encounter.characters[1].effectiveCharacterLevel;
    var iParty3Level = encounter.characters[2].effectiveCharacterLevel;
    var iParty4Level = encounter.characters[3].effectiveCharacterLevel;
    var iParty5Level = encounter.characters[4].effectiveCharacterLevel;
    var iParty6Level = encounter.characters[5].effectiveCharacterLevel;

    var iMonster1Number = encounter.monsters[0].quantity;
    var iMonster2Number = encounter.monsters[1].quantity;
    var iMonster3Number = encounter.monsters[2].quantity;
    var iMonster4Number = encounter.monsters[3].quantity;
    var iMonster5Number = encounter.monsters[4].quantity;
    var iMonster6Number = encounter.monsters[5].quantity;

    var iMonster1Level = encounter.monsters[0].challengeRating;
    var iMonster2Level = encounter.monsters[1].challengeRating;
    var iMonster3Level = encounter.monsters[2].challengeRating;
    var iMonster4Level = encounter.monsters[3].challengeRating;
    var iMonster5Level = encounter.monsters[4].challengeRating;
    var iMonster6Level = encounter.monsters[5].challengeRating;

    var iParty1Power = iParty1Number * this.mCRtoPL(iParty1Level);
    var iParty2Power = iParty2Number * this.mCRtoPL(iParty2Level);
    var iParty3Power = iParty3Number * this.mCRtoPL(iParty3Level);
    var iParty4Power = iParty4Number * this.mCRtoPL(iParty4Level);
    var iParty5Power = iParty5Number * this.mCRtoPL(iParty5Level);
    var iParty6Power = iParty6Number * this.mCRtoPL(iParty6Level);
    var iPartyTotalPower = (iParty1Power + iParty2Power + iParty3Power + iParty4Power + iParty5Power + iParty6Power) / 4;
    var iPartyEffectiveLevel = this.mPLtoCR(iPartyTotalPower);

    var iPartyTotal = iParty1Number + iParty2Number + iParty3Number + iParty4Number + iParty5Number + iParty6Number;
    iCount += iParty1Number * iParty1Level;
    iCount += iParty2Number * iParty2Level;
    iCount += iParty3Number * iParty3Level;
    iCount += iParty4Number * iParty4Level;
    iCount += iParty5Number * iParty5Level;
    iCount += iParty6Number * iParty6Level;
    var iPartyAverageLevel = iCount / iPartyTotal;

    var iMonster1Power = iMonster1Number * this.mCRtoPL(iMonster1Level);
    var iMonster2Power = iMonster2Number * this.mCRtoPL(iMonster2Level);
    var iMonster3Power = iMonster3Number * this.mCRtoPL(iMonster3Level);
    var iMonster4Power = iMonster4Number * this.mCRtoPL(iMonster4Level);
    var iMonster5Power = iMonster5Number * this.mCRtoPL(iMonster5Level);
    var iMonster6Power = iMonster6Number * this.mCRtoPL(iMonster6Level);
    var iMonsterTotalPower = iMonster1Power + iMonster2Power + iMonster3Power + iMonster4Power + iMonster5Power + iMonster6Power;
    var iMonsterTotalLevel = this.mPLtoCR(iMonsterTotalPower);

    var iDifference = this.mDifference(iMonsterTotalPower, iPartyTotalPower);

    var iMonster1Experience = iMonster1Number * this.mExperience(iPartyAverageLevel, iMonster1Level);
    var iMonster2Experience = iMonster2Number * this.mExperience(iPartyAverageLevel, iMonster2Level);
    var iMonster3Experience = iMonster3Number * this.mExperience(iPartyAverageLevel, iMonster3Level);
    var iMonster4Experience = iMonster4Number * this.mExperience(iPartyAverageLevel, iMonster4Level);
    var iMonster5Experience = iMonster5Number * this.mExperience(iPartyAverageLevel, iMonster5Level);
    var iMonster6Experience = iMonster6Number * this.mExperience(iPartyAverageLevel, iMonster6Level);
    var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
    var iCRExperience = iMonsterTotalExperience / iPartyTotal;

    var iCR1Experience = 1;
    if (iParty1Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty1Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty1Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty1Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty1Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty1Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty1Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR1Experience = iMonsterTotalExperience / iPartyTotal;
    };

    var iCR2Experience = 0;
    if (iParty2Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty2Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty2Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty2Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty2Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty2Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty2Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR2Experience = iMonsterTotalExperience / iPartyTotal;
    };

    var iCR3Experience = 0;
    if (iParty3Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty3Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty3Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty3Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty3Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty3Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty3Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR3Experience = iMonsterTotalExperience / iPartyTotal;
    };

    var iCR4Experience = 0;
    if (iParty4Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty4Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty4Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty4Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty4Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty4Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty4Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR4Experience = iMonsterTotalExperience / iPartyTotal;
    };

    var iCR5Experience = 0;
    if (iParty5Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty5Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty5Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty5Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty5Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty5Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty5Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR5Experience = iMonsterTotalExperience / iPartyTotal;
    };

    var iCR6Experience = 0;
    if (iParty6Number > 0) {
      var iMonster1Experience = iMonster1Number * this.mExperience(iParty6Level, iMonster1Level);
      var iMonster2Experience = iMonster2Number * this.mExperience(iParty6Level, iMonster2Level);
      var iMonster3Experience = iMonster3Number * this.mExperience(iParty6Level, iMonster3Level);
      var iMonster4Experience = iMonster4Number * this.mExperience(iParty6Level, iMonster4Level);
      var iMonster5Experience = iMonster5Number * this.mExperience(iParty6Level, iMonster5Level);
      var iMonster6Experience = iMonster6Number * this.mExperience(iParty6Level, iMonster6Level);
      var iMonsterTotalExperience = iMonster1Experience + iMonster2Experience + iMonster3Experience + iMonster4Experience + iMonster5Experience + iMonster6Experience;
      iCR6Experience = iMonsterTotalExperience / iPartyTotal;
    };

    let result: EncounterResult = {
      partyLevel: this.RoundOnePlace(iPartyEffectiveLevel),
      encounterLevel: Math.round(iMonsterTotalLevel),
      difficulty: DifficultyType.VeryDifficult, // todo: DifficultyType[this.mDifficulty(iDifference) as keyof typeof DifficultyType],
      percentOfTotal: parseInt(this.mPercentEnc(iDifference)),
      treasureValue: this.RoundOnePlace(this.mTreasure(iMonsterTotalLevel)),
      experiencePoints: [
        Math.round(iCR1Experience),
        Math.round(iCR2Experience),
        Math.round(iCR3Experience),
        Math.round(iCR4Experience),
        Math.round(iCR5Experience),
        Math.round(iCR6Experience)
      ]
    };

    if (!!encounter.terrain) {
      result.spotChecks = this.findSpotDistance(encounter.terrain);
    }

    return result;
  }
}
export default Rencontre;