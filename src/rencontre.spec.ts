import { should } from 'chai';
import { DifficultyType, Encounter, EncounterResult } from './rencontre.core';
import { Rencontre } from './rencontre';

should();

describe('Rencontre test suite', function () {
  let rencontre = new Rencontre();
  it('Can create an encounter', function () {
    let result = rencontre.create({
      characters: [
        { quantity: 1, effectiveCharacterLevel: 1 }
        , { quantity: 1, effectiveCharacterLevel: 1 }
        , { quantity: 1, effectiveCharacterLevel: 1 }
        , { quantity: 1, effectiveCharacterLevel: 1 }
        , { quantity: 1, effectiveCharacterLevel: 1 }
        , { quantity: 1, effectiveCharacterLevel: 1 }
      ],
      monsters: [
        { quantity: 1, challengeRating: 1 }
        , { quantity: 1, challengeRating: 1 }
        , { quantity: 1, challengeRating: 1 }
        , { quantity: 1, challengeRating: 1 }
        , { quantity: 1, challengeRating: 1 }
        , { quantity: 1, challengeRating: 1 }
      ]
    });
    result.partyLevel.should.equals(1.5
      , `Should return: 1.5, but returned: ${result.partyLevel}`);
    result.encounterLevel.should.equals(5
      , `Should return: 5, but returned: ${result.encounterLevel}`);
    result.difficulty.should.equals(DifficultyType.VeryDifficult
      , `Should return: Very Difficult, but returned: ${result.difficulty}`);
    result.percentOfTotal.should.equals(15
      , `Should return: 15, but returned: ${result.percentOfTotal}`);
    result.treasureValue.should.equals(1668
      , `Should return: 1668, but returned: ${result.treasureValue}`);
    result.experiencePoints[0].should.equals(300
      , `Should return: 300, but returned: ${result.experiencePoints[0]}`);
  });
});